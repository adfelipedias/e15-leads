# Uma CRUD para controle de Lead
## Lead são pessoas que podem estar interessados em algum tipo de produto ou serviço.

## Tecnologias utilzadas:
- Python
- Python Dotenv
- Flask
- Flask Migrate
- Flask SQLAlchemy
- SQLAlchemy
- Pyscopg2
- Padrão Factory
- PostegreSQL
- Blueprints


## Rotas:
POST "/lead" - registra um novo Lead no banco de dados, creation_date e last_visit são preenchidos no momento da criação.

GET "/lead" - Lista todos os Leads por ordem de visita, do maior pelo menor.

PATCH "/lead" - É utilizado o email para encontrar o registro, essa rota atualiza apenas o valor de visits e last_visit em cada requisição.

DELETE "/lead" - Deleta uma Lead específica, o email é utilizado para encontrar o registro



