from werkzeug.exceptions import BadRequest


class InvalidPhone(BadRequest):
    ...


class InvalidField(BadRequest):
    ...
