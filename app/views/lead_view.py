from flask import Blueprint, jsonify
import sqlalchemy

from app.services.lead_services import register_lead, update_lead, verify_phone, delete
from app.exceptions.lead_exception import InvalidField, InvalidPhone
from app.models.lead_model import LeadModel

bp_lead = Blueprint("lead", __name__, url_prefix="/lead")


@bp_lead.post("")
def create():
    try:
        lead = register_lead()
        return jsonify(lead)
    except InvalidPhone as err:
        return err.description, err.code
    except sqlalchemy.exc.IntegrityError as err:
        return {"msg": str(err.orig).split("\n")[0]}, 400


@bp_lead.get("")
def get_all():
    lead = LeadModel.query.all()
    return jsonify(lead)


@bp_lead.patch("")
def update():
    try:
        lead = update_lead()
        return jsonify(lead)
    except InvalidField as err:
        return err.description, err.code


@bp_lead.delete("")
def delete_lead():
    lead = delete()
    return jsonify(lead), 204
