from flask import current_app, request
from datetime import datetime
import re

from app.exceptions.lead_exception import InvalidField, InvalidPhone
from app.models.lead_model import LeadModel


def verify_phone(phone):
    result = re.fullmatch("\(\d{2}\)\d{4,5}\-\d{4}", phone)

    if not result:
        raise InvalidPhone(
            "The phone is invalid format - correct is (xx)xxxxx-xxxx"
        )

    return result


def connect_db(lead):
    session = current_app.db.session
    session.add(lead)
    session.commit()


def register_lead():
    time_hour = datetime.today().strftime("%Y-%m-%d")
    data = request.get_json()

    verify_phone(data["phone"])

    lead = LeadModel(
        name=data["name"],
        email=data["email"],
        phone=data["phone"],
        creation_date=time_hour,
        last_visit=time_hour,
    )

    connect_db(lead)
    return lead


def update_lead():
    data = request.get_json()
    num_max_field = 1
    add_visit = 1

    if len(data) != num_max_field or "email" not in data.keys():
        raise InvalidField("Invalid Field.")

    lead = LeadModel.query.filter_by(email=data["email"]).first_or_404()

    new_visit = int(lead.visits) + add_visit
    lead.visits = new_visit

    new_time_hour = datetime.today().strftime("%Y-%m-%d")
    lead.last_visit = new_time_hour

    for key, value in data.items():
        setattr(lead, key, value)

    connect_db(lead)
    return lead


def delete():
    data = request.get_json()
    lead = LeadModel.query.filter_by(email=data["email"]).first_or_404()

    connect_db(lead)
    return lead
