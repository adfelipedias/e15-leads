from sqlalchemy import Column, String, DateTime, Integer
from dataclasses import dataclass
from datetime import datetime

from app.configs.database import db


@dataclass
class LeadModel(db.Model):
    id: int
    name: str
    email: str
    phone: str
    creation_date: datetime
    last_visit: datetime
    visits: int

    __tablename__ = "leads"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    email = Column(String, nullable=False, unique=True)
    phone = Column(String, nullable=False, unique=True)
    creation_date = Column(DateTime, nullable=False)
    last_visit = Column(DateTime, nullable=False)
    visits = Column(Integer, default=1)
